OKD console configuration
======================

This chart configures the OKD Console via the console-operator.

E.g. configure a custom Route hostname and certificate for the console.
